import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_music_app/config/net/base_api.dart';
import 'package:flutter_music_app/config/router_manager.dart';
import 'package:flutter_music_app/model/full_data_model.dart';
import 'package:flutter_music_app/model/song_model.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BaseRepository {
  static Future fetchSongList() async {
    
    final box = GetStorage();
    final token = box.read('token');
    try {
      var response = await http.get('/api/data',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));

      return FullDataModel.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        Get.offAllNamed(RouteName.login);
      }
    }
  }
}
