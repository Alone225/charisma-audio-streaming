import 'package:flutter_music_app/model/full_data_model.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class FullModelController extends GetxController {
  final Rx<FullDataModel> _obj = Rx<FullDataModel>();
  set fullModel(value) => _obj.value = value;
  FullDataModel get fullModel => _obj.value;
}
