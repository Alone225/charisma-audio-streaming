import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_music_app/anims/page_route_anim.dart';
import 'package:flutter_music_app/config/net/base_api.dart';
import 'package:flutter_music_app/ui/page/tab/tab_navigator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _emailController;
  TextEditingController _passwordController;
  final box = GetStorage();
  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void login() async {
    try {
      var response = await http.post('/api/me',
          data: {
            'email': _emailController.text,
            'password': _passwordController.text
          },
          options: Options(
            contentType: Headers.jsonContentType,
          ));
      debugPrint(response.data['token']);
      box.write('token', response.data['token']);

      Navigator.of(context).pushReplacement(NoAnimRouteBuilder(TabNavigator()));
    } on DioError catch (e) {
      print(e);
      Get.snackbar('Erreur', 'Identifiants incorrects');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text('Connexion',
                style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.2)),
          ),
          Expanded(
              child: Center(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
              Image(image: AssetImage('assets/icon/icon.png', ), width: 100, height: 100,),
              SizedBox(height: 50,),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor.withAlpha(50),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: TextField(
                  controller: _emailController,
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.grey,
                  ),
                  onChanged: (value) {},
                  onSubmitted: (value) {},
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.grey,
                      ),
                      hintText: 'Email'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor.withAlpha(50),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: TextField(
                  obscureText: true,
                  controller: _passwordController,
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.grey,
                  ),
                  onChanged: (value) {},
                  onSubmitted: (value) {},
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.lock,
                        color: Colors.grey,
                      ),
                      hintText: 'Mot de passe'),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              ButtonTheme(
                minWidth: 300.0,
                height: 50.0,
                child: RaisedButton(
                  color: Theme.of(context).accentColor,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  onPressed: () {
                    login();
                  },
                  child: Text("Connexion"),
                ),
              )
                ],
              ),
            ),
          )),
        ],
      ),
    ));
  }
}
