import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_music_app/generated/i18n.dart';
import 'package:flutter_music_app/model/download_model.dart';
import 'package:flutter_music_app/model/favorite_model.dart';
import 'package:flutter_music_app/model/song_model.dart';
import 'package:flutter_music_app/provider/provider_widget.dart';
import 'package:flutter_music_app/ui/page/player_page.dart';
import 'package:flutter_music_app/ui/widget/all_songs_list.dart';
import 'package:flutter_music_app/ui/widget/song_list_carousel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class MusicPage extends StatefulWidget {
  @override
  _MusicPageState createState() => _MusicPageState();
}

class _MusicPageState extends State<MusicPage> {
  @override
  void initState() {
    loadData();
    super.initState();
  }

  loadData() async {
    final dir = await getApplicationDocumentsDirectory();
    List<FileSystemEntity> files = dir.listSync();
    for (var file in files) {
      print(file.path);
    }
    setState(() {});
  }

  Widget _buildSongItem(Song data) {
    FavoriteModel favoriteModel = Provider.of(context);
    SongModel songModel = Provider.of(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12.0),
            child: Container(
                width: 50, height: 50, child: Image.network(data.pic)),
          ),
          SizedBox(
            width: 20.0,
          ),
          Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data.title,
                    style: data.url == null
                        ? TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey,
                          )
                        : TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600,
                          ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    data.author,
                    style: data.url == null
                        ? TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey,
                          )
                        : TextStyle(
                            fontSize: 10.0,
                          ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ]),
          ),
          IconButton(
              onPressed: () => favoriteModel.collect(data),
              icon: data.url == null
                  ? Icon(
                      Icons.favorite_border,
                      color: Color(0xFFE0E0E0),
                      size: 20.0,
                    )
                  : favoriteModel.isCollect(data)
                      ? Icon(
                          Icons.favorite,
                          color: Theme.of(context).accentColor,
                          size: 20.0,
                        )
                      : Icon(
                          Icons.favorite_border,
                          size: 20.0,
                        ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SongModel songModel = Provider.of(context);
    return ProviderWidget<SongListModel>(
        onModelReady: (model) async {
          await model.initData();
        },
        model: SongListModel(),
        builder: (context, model, child) {
          return SafeArea(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      IconButton(
                          icon: Icon(Icons.sort),
                          onPressed: () {
                            Scaffold.of(context).openDrawer();
                          }),
                      Text('Tout les audios',
                          style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1.2)),
                    ],
                  ),
                ),
                Expanded(
                    child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: model.list.length,
                  itemBuilder: (BuildContext context, int index) {
                    Song data = model.list[index];
                    return GestureDetector(
                      onTap: () {
                        if (null != data.url) {
                          SongModel songModel = Provider.of(context);
                          songModel.setSongs(new List<Song>.from(model.list));
                          songModel.setCurrentIndex(index);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => PlayPage(
                                nowPlay: true,
                              ),
                            ),
                          );
                        }
                      },
                      child: _buildSongItem(data),
                    );
                  },
                ))
              ]));
        });
  }
}
