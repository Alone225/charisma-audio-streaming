import 'package:flutter/material.dart';
import 'package:flutter_music_app/getx_controllers/FullModelController.dart';
import 'package:flutter_music_app/model/full_data_model.dart';
import 'package:flutter_music_app/model/home_model.dart';
import 'package:flutter_music_app/ui/widget/albums_carousel.dart';
import 'package:flutter_music_app/ui/widget/app_bar.dart';
import 'package:flutter_music_app/ui/widget/artist_carousel.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class ArtistPage extends StatefulWidget {
  final Artists artist;

  ArtistPage({Key key, this.artist}) : super(key: key);

  @override
  _ArtistPageState createState() => _ArtistPageState();
}

class _ArtistPageState extends State<ArtistPage> {
  FullModelController _fullModelController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          AppBarCarousel(),
          Expanded(
            child: ListView(
              children: <Widget>[
                Center(
                    child: Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: MediaQuery.of(context).size.width * 0.5,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child:
                          Container(child: Image.network(widget.artist.image))),
                )),
                SizedBox(height: 20.0),
                Center(
                  child: Text(
                    widget.artist.name,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
                /*Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 70,
                        margin: EdgeInsets.only(
                            top: 20, bottom: 20, left: 20, right: 10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12, width: 1),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.play_arrow,
                              color: Theme.of(context).accentColor,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Play',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 70,
                        margin: EdgeInsets.only(
                            top: 20, bottom: 20, left: 10, right: 20),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12, width: 1),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.add),
                            SizedBox(
                              width: 5,
                            ),
                            Text('Add'),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),*/
                ArtistCarousel(
                  input: widget.artist.name,
                  artistId: widget.artist.id,
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
