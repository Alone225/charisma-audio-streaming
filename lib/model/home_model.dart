import 'dart:math';

import 'package:flutter_music_app/model/full_data_model.dart';
import 'package:flutter_music_app/model/song_model.dart';
import 'package:flutter_music_app/provider/view_state_refresh_list_model.dart';
import 'package:flutter_music_app/service/base_repository.dart';

class HomeModel extends ViewStateRefreshListModel {
  List<Albums> _albums;
  List<Song> _forYou;
  List<Artists> _artists;
  FullDataModel _fullDataModel;
  List<Albums> get albums => _albums;
  List<Artists> get artists => _artists;
  List<Song> get forYou => _forYou;

  FullDataModel get fullData => _fullDataModel;
  @override
  Future<List<Song>> loadData({int pageNum}) async {
    List<Future> futures = [];

    futures.add(BaseRepository.fetchSongList());

    FullDataModel result = await BaseRepository.fetchSongList();
    _albums = result.albums;
    _forYou = result.recentlyPlayed;
    _artists = result.artists;
    _fullDataModel = result;
    return result.songs;
  }
}
