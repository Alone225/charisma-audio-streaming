import 'package:flutter_music_app/model/song_model.dart';

class FullDataModel {
  List<Albums> albums;
  List<Artists> artists;
  List<Song> songs;
  Settings settings;
  List<Song> playlists;

  List<String> recentlyPlayedlist;
  List<Users> users;
  Users currentUser;
  bool useLastfm;
  bool useYouTube;
  bool useiTunes;
  bool allowDownload;
  bool supportsTranscoding;
  String cdnUrl;
  String currentVersion;
  String latestVersion;

  List<Song> get recentlyPlayed {
    return songs
        .where((element) => recentlyPlayedlist.contains(element.id))
        .toList();
  }

  FullDataModel(
      {this.albums,
      this.artists,
      this.songs,
      this.settings,
      this.playlists,
      this.recentlyPlayedlist,
      this.users,
      this.currentUser,
      this.useLastfm,
      this.useYouTube,
      this.useiTunes,
      this.allowDownload,
      this.supportsTranscoding,
      this.cdnUrl,
      this.currentVersion,
      this.latestVersion});

  FullDataModel.fromJson(Map<String, dynamic> json) {
    if (json['albums'] != null) {
      albums = new List<Albums>();
      json['albums'].forEach((v) {
        albums.add(new Albums.fromJson(v));
      });
    }
    if (json['artists'] != null) {
      artists = new List<Artists>();
      json['artists'].forEach((v) {
        artists.add(new Artists.fromJson(v));
      });
    }
    if (json['songs'] != null) {
      songs = new List<Song>();
      var authorsMaps = json['artists'];

      json['songs'].forEach((v) {
        v.putIfAbsent(
            'author',
            () => authorsMaps
                .firstWhere((element) => element['id'] == v['artist_id']));
        songs.add(new Song.fromJsonMap(v));
      });
    }
    settings = json['settings'] != null
        ? new Settings.fromJson(json['settings'])
        : null;
    if (json['playlists'] != null) {
      playlists = new List<Song>();
      json['playlists'].forEach((v) {
        playlists.add(new Song.fromJsonMap(v));
      });
    }

    if (json['recentlyPlayed'] != null) {
      recentlyPlayedlist = new List<String>();
      json['recentlyPlayed'].forEach((v) {
        recentlyPlayedlist.add(v);
      });
    }
    if (json['users'] != null) {
      users = new List<Users>();
      json['users'].forEach((v) {
        users.add(new Users.fromJson(v));
      });
    }
    currentUser = json['currentUser'] != null
        ? new Users.fromJson(json['currentUser'])
        : null;
    useLastfm = json['useLastfm'];
    useYouTube = json['useYouTube'];
    useiTunes = json['useiTunes'];
    allowDownload = json['allowDownload'];
    supportsTranscoding = json['supportsTranscoding'];
    cdnUrl = json['cdnUrl'];
    currentVersion = json['currentVersion'];
    latestVersion = json['latestVersion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.albums != null) {
      data['albums'] = this.albums.map((v) => v.toJson()).toList();
    }
    if (this.artists != null) {
      data['artists'] = this.artists.map((v) => v.toJson()).toList();
    }
    if (this.songs != null) {
      data['songs'] = this.songs.map((v) => v.toJson()).toList();
    }
    if (this.settings != null) {
      data['settings'] = this.settings.toJson();
    }
    if (this.playlists != null) {
      data['playlists'] = this.playlists.map((v) => v.toJson()).toList();
    }

    if (this.recentlyPlayed != null) {
      data['recentlyPlayed'] =
          this.recentlyPlayed.map((v) => v.toJson()).toList();
    }
    if (this.users != null) {
      data['users'] = this.users.map((v) => v.toJson()).toList();
    }
    if (this.currentUser != null) {
      data['currentUser'] = this.currentUser.toJson();
    }
    data['useLastfm'] = this.useLastfm;
    data['useYouTube'] = this.useYouTube;
    data['useiTunes'] = this.useiTunes;
    data['allowDownload'] = this.allowDownload;
    data['supportsTranscoding'] = this.supportsTranscoding;
    data['cdnUrl'] = this.cdnUrl;
    data['currentVersion'] = this.currentVersion;
    data['latestVersion'] = this.latestVersion;
    return data;
  }
}

class Albums {
  int id;
  int artistId;
  String name;
  String cover;
  String createdAt;
  bool isCompilation;

  Albums(
      {this.id,
      this.artistId,
      this.name,
      this.cover,
      this.createdAt,
      this.isCompilation});

  Albums.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    artistId = json['artist_id'];
    name = json['name'];
    cover = json['cover'];
    createdAt = json['created_at'];
    isCompilation = json['is_compilation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['artist_id'] = this.artistId;
    data['name'] = this.name;
    data['cover'] = this.cover;
    data['created_at'] = this.createdAt;
    data['is_compilation'] = this.isCompilation;
    return data;
  }
}

class Artists {
  int id;
  String name;
  String image;

  Artists({this.id, this.name, this.image});

  Artists.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = 'https://test-music.hostapp.fr/public/images/logo.png';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}

class Settings {
  String mediaPath;

  Settings({this.mediaPath});

  Settings.fromJson(Map<String, dynamic> json) {
    mediaPath = json['media_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['media_path'] = this.mediaPath;
    return data;
  }
}

class Users {
  int id;
  String name;
  String email;
  bool isAdmin;
  List<Null> preferences;

  Users({this.id, this.name, this.email, this.isAdmin, this.preferences});

  Users.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    isAdmin = json['is_admin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['is_admin'] = this.isAdmin;

    return data;
  }
}
